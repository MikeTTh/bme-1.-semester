#include <stdio.h>

char gray[][4] = {"000", "001", "011", "010", "110", "111", "101", "100"};
char dk[][4] = {"000", "001", "111", "011", "101", "100", "110", "010"};

void csere(int miben[], int mit, int mivel){
    int t = miben[mit];
    miben[mit] = miben[mivel];
    miben[mivel] = t;
}

void graykiir(int sorrend[]){
    printf("Permutáció: ");
    for (int i = 0; i < 3; ++i) {
        printf("%d", sorrend[i]);
    }
    printf("\nGray DKód Hamm\n");
    int hammossz = 0;
    for (int i = 0; i < 8; ++i) {
        int hamm = 0;
        for (int j = 0; j < 3; ++j) {
            if (gray[i][sorrend[j]] != dk[i][j])
                hamm++;
            printf("%c", gray[i][sorrend[j]]);
        }
        hammossz += hamm;
        printf("  %s  ", dk[i]);
        printf("%d", hamm);
        printf("\n");
    }
    printf("Össz táv: %d\n", hammossz);
    printf("\n");
}

void permutal(int sorrend[], int l, int r){
    if (l == r){
        graykiir(sorrend);
    } else {
        for (int i = l; i < r; i++)
        {
            csere(sorrend, l, i);
            permutal(sorrend, l+1, r);
            csere(sorrend, l, i);
        }
    }
}

int main() {
    int sorrend[] = {0, 1, 2};

    permutal(sorrend, 0, 3);
    return 0;
}
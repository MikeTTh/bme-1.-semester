DEF LD  0x80                ; LED adatregiszter (�rhat�/olvashat�)

data
    DIGIT_CODE:
        db 0x18, 0x7A, 0x38, 0x59, 0x4A, 0x68, 0x20
    
code
    start:
        mov r0, #DIGIT_CODE ; Digit k�d "t�mb" elso elem�nek c�m�nek bet�lt�se r0-ba
        mov r1, #7          ; H�tral�vo elemek sz�m�nak bet�lt�se
        mov r2, #0          ; Az �sszeget sz�ml�l� regiszter inicializ�l�sa 0-val
    read:                   ; A byte-onk�nt halad� loop
        mov r8, (r0)        ; r8-ba bet�lt�nk egy bitet a digit k�db�l
        sl0 r8              ; balra shiftelj�k
        bit_loop:           ; A bitenk�nt halad� loop
            adc r2, #0      ; A kil�po bitet hozz�adom az egyesek sz�m�t tartalmaz� regiszterhez
            sl0 r8          ; balra shiftel�nk
            jnz bit_loop    ; Ha m�g van 1-es az adott byte-ban
        adc r2, #0          ; Az utols� kil�po bitet is hozz�adjuk az �sszeghez
        add r0, #1          ; A k�vetkezo elem c�m�re �ll�tjuk r0-t
        sub r1, #1          ; A h�tral�vo elemek sz�m�t cs�kkentj�k
        jnz read            ; Ha van m�g elem loopolunk
    mov LD, r2              ; A ledekre kirakjuk az �sszeget
    halt:
        jmp halt            ; Meg�ll�tjuk a processzort
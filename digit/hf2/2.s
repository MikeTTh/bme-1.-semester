DEF LD  0x80                ; LED adatregiszter (�rhat�/olvashat�)

data
    DIGIT_CODE:
        db 0x18, 0x7A, 0x38, 0x59, 0x4A, 0x68, 0x20
        
    org 0x10
    SUM_LUT:
        db 0x00, 0x01, 0x01, 0x02, 0x01, 0x02, 0x02, 0x03, 0x01, 0x02, 0x02, 0x03, 0x02, 0x03, 0x03, 0x04

code
    start:
        mov r0, #DIGIT_CODE      ; Bet�ltj�k a digit k�d elso byte-j�nak c�m�t
        mov r1, #7               ; Bet�ltj�k a h�tral�vo adatok sz�m�t
        mov r2, #0               ; 0-val inicializ�ljuk az �sszeget tartalmaz� regisztert
        read:                    ; A byte-onk�nt lefut� ciklus
            mov r8, (r0)         ; Bet�lt�nk egy byte-ot a digit k�db�l
            and r8, #0x0F        ; Maszkolunk, hogy csak az als� 4 bit maradjon
            mov r9, #SUM_LUT     ; Bet�ltj�k a t�bla kezdet�nek hely�t
            add r9, r8           ; A t�bla c�m�t tart. regisztert a jelenlegi szjegy. �rt�k�vel l�ptetj�k
            mov r10, (r9)        ; Bet�ltj�k az ott l�vo �rt�ket
            add r2, r10          ; Hozz�adjuk az �sszegehez
        
            mov r8, (r0)         ; �jra bet�lt�nk egy byte-ot a digit k�db�l
            swp r8               ; Megcser�lj�k a felso �s als� 4 bitet
            and r8, #0x0F        ; Ugyanazt megcsin�ljuk mint az als� 4 bittel
            mov r9, #SUM_LUT
            add r9, r8
            mov r10, (r9)
            add r2, r10
        
            add r0, #1           ; A k�vekezo byte c�m�re �ll�tjuk r0-t
            sub r1, #1           ; Cs�kkentj�k a h�tral�vo adatok sz�m�t
            jnz read             ; Ha van m�g h�tra adat, akkor loopolunk
        mov LD, r2               ; Ki�rjuk a ledekre az �sszeget
        halt:
            jmp halt             ; Meg�ll�tjuk a processzort
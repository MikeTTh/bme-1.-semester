DEF LD  0x80                ; LED adatregiszter (�rhat�/olvashat�)

data
    DIGIT_CODE:
        db 0x18, 0x7A, 0x38, 0x59, 0x4A, 0x68, 0x20
        
code
    start:
        mov r0, #DIGIT_CODE   ; Digit k�d elej�nek c�m�nek bet�lt�se
        mov r1, #7            ; A h�tral�vo elemek sz�m�nak bet�lt�se
        mov r2, #0            ; Az �sszeg sz�ml�l� inicializ�l�sa 0-val
        
        read:
            mov r8, (r0)      ; Egy elem bet�lt�se
            mov r9, r8        ; �s m�sol�sa
            and r8, #0x55     ; Csak a p�ratlan szjegyek megtart�sa
            and r9, #0xAA     ; Csak a p�ros szjegyek megtart�sa
            sr0 r9            ; A p�ros elemek shiftel�se jobbra
            add r8, r9        ; A p�ros �s ptlan elemek �sszead�sa
            mov r9, r8        ; Ennek az �sszegnek m�sol�sa a m�sik munkaregiszterbe is
        
            and r8, #0x33     ; K�t sz�mjegyenk�nt ugyanazt csin�ljuk
            and r9, #0xCC
            sr0 r9            ; De most 2x l�ptet�nk
            sr0 r9
            add r8, r9        ; �sszeadjuk
            mov r9, r8        ; M�soljuk megint mindk�t munkaregiszterbe
        
            and r8, #0x0F     ; Most n�gy sz�mjegyenk�t tessz�k ugyanazt
            and r9, #0xF0
            swp r9            ; Most n�gyszer k�ne l�ptetni, de a swp az ekvivalens �s hat�konyabb
            add r8, r9        ; Megint �sszeadjuk oket
            
            add r2, r8        ; A byteban l�vo egyesek sz�m�t hozz�adjuk az �sszeghez
            add r0, #1        ; A k�vetkezo elem c�m�re l�p�nk
            sub r1, #1        ; Cs�kkentj�k a h�tral�vo elemek sz�m�t
            jnz read          ; Loopolunk
        
        mov LD, r2            ; Kitessz�k a ledekre az eredm�nyt
        
        halt:
            jmp halt          ; Meg�ll�tjuk a processzort
DEF LD  0x80                ; LED adatregiszter (�rhat�/olvashat�)

data
    DIGIT_CODE:
        db 0x18, 0x7A, 0x38, 0x59, 0x4A, 0x68, 0x20
        
code
    start:
        mov r0, #DIGIT_CODE  ; Bet�ltj�k a digit k�d elso byte-j�nak c�m�t
        mov r1, #7           ; Bet�ltj�k a h�tral�vo adatok sz�m�t
        mov r2, #0           ; Inicializ�ljuk az �sszeget 0-ra
        
    tolt:
        mov r8, (r0)         ; Bet�lt�nk egy byte-ot a digitk�db�l
        cmp r8, #0
        jz kov               ; Ha a byte == 0, akkor k�vetkezo
        mov r9, r8           ; Lem�soljuk
        
        loop:
            sub r9, #1       ; Az egyikbol kivonunk egyet
            and r8, r9       ; A kettot �ssze�seljuk
            jz betolt        ; Ha 0, akkor nem volt benne m�r egyes, k�v. byte
            add r2, #1       ; Ha volt benne egyes, akkor hozz�adunk az �sszeghez
            mov r9, r8       ; M�soljuk a megv�ltozott �rt�ket
            jmp loop         ; Ezt annyiszor ism�telj�k, ah�ny egyes van a byte-ban
            
        betolt:
            add r2, #1       ; A loopban eggyel kevesebbet adunk hozz�, +1
        kov:
            add r0, #1       ; L�p�nk a k�vetkezo byte-ra 
            sub r1, #1       ; Kivonunk a h�tral�vo elemek sz�m�b�l
            jz kiir          ; Ha nincs elem h�tra kitessz�k a ledekre
            jmp tolt         ; M�s esetben bet�ltj�k a k�vetkezot
        
        kiir:
            mov LD, r2       ; Ledekre ki�r�s
            halt:
                jmp halt     ; Processzor meg�ll�t�sa

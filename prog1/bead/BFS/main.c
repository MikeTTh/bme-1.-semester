#include <stdio.h>
#include <stdlib.h>

typedef struct faelem{
    int data;
    struct faelem* bal;
    struct faelem* jobb;
}faelem;

typedef struct queueNode{
    faelem* fa;
    struct queueNode* next;
}queueNode;

typedef struct queue {
    queueNode* start;
    queueNode* end;
} queue;

faelem* new_faelem(){
    faelem* root = malloc(sizeof(faelem));
    root->bal = NULL;
    root->jobb = NULL;
    root->data = -1;
    return root;
}

queueNode* new_queueNode(faelem* fa){
    queueNode* q = malloc(sizeof(queueNode));
    q->next = NULL;
    q->fa = fa;
    return q;
}

// Keresőfa, mert miért ne
void beszur(faelem** fa, int num){
    if (*fa == NULL){
        *fa = new_faelem();
        (*fa)->data = num;
    } else if (num > (*fa)->data){
        beszur(&(*fa)->jobb, num);
    } else if (num < (*fa)->data){
        beszur(&(*fa)->bal, num);
    }
}

queue newQueue(faelem* fa){
    queueNode* q = new_queueNode(fa);
    return (queue){q, q};
}

void sorfuz(queue* q, faelem* fa){
    if (q->end == NULL) {
        q->end = new_queueNode(fa);
    } else {
        q->end->next = new_queueNode(fa);
        q->end = q->end->next;
    }
    if(q->start == NULL)
        q->start = q->end;
}

/*
 * Mint ahogy a futószal(l)agról is az elején lévő elemet
 * leveszik és továbbadják és az őt hordozó szalagrész is "felszabadul"
 * => futószalag
 * (igazából csak azért ez a neve, mert ha az lenne, hogy sorLeptet,
 * akkor vissza lenne dobva mert fel is szabadítom és ennél nem jut kreatívabb név az eszembe.)
 */
faelem* futoszalag(queue* q){
    queueNode* temp = q->start;
    faelem* ret = temp->fa;
    q->start = q->start->next;
    if (q->start == NULL) // Vége a sornak (eleje==vége volt felszabadítva)
        q->end = NULL;
    free(temp);
    return ret;
}

void BFS(faelem* fa){
    queue q = newQueue(fa);
    while (q.start != NULL){
        faelem* csomopont = futoszalag(&q);
        if (csomopont->bal != NULL){
            sorfuz(&q, csomopont->bal);
        }
        if (csomopont->jobb != NULL){
            sorfuz(&q, csomopont->jobb);
        }
        printf("%d\n", csomopont->data);
    }
}

void freeTree(faelem* root){
    if (root == NULL)
        return;
    freeTree(root->bal);
    freeTree(root->jobb);
    free(root);
}

int main() {
    int nums[] = {3, 1, 0, 2, 5, 4, 6};
    faelem* fa = NULL;
    for (int i = 0; i < 7; ++i) {
        beszur(&fa, nums[i]);
    }
    BFS(fa);
    freeTree(fa);
    return 0;
}
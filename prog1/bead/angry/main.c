#include <stdio.h>
#include <math.h>
#include <stdbool.h>

// 40m széles
#define szeles 40.0

// 10m magas
#define magas 20.0

// 20px/m
#define arany 20.0

#define madar 5, 2
#define disznaja 35, 7
#define r 0.5

#define g 9.81
#define szogstart 10
#define szogstop 60
#define szoglep 5
#define vstart 10
#define vstop 30
#define vlep 1
#define tszamol 0.001
#define trajzol 0.01

#define PI acos(-1.0)

typedef struct {
    double X, Y;
} position;

typedef struct {
    double szog, seb;
} palya;

double mToPx(double m){
    return m*arany;
}

void palyaRajz(FILE *f, palya p, position start, double stop);

bool joPalya(palya p, position start, position stop);

int main() {
    FILE *f = fopen("angry.svg", "w");
    fprintf(f, "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"%.0f\" height=\"%.0f\">\n", mToPx(szeles), mToPx(magas));

    position diszno = {disznaja};
    position red = {madar};

    for (double fok = szogstart; fok < szogstop; fok += szoglep) {
        for (double seb = vstart; seb < vstop; seb+=vlep) {
            palya mostani = {.szog =  fok, .seb = seb};
            if (joPalya(mostani, red, diszno)) {
                palyaRajz(f, mostani, red, diszno.X);
            }
        }
    }

    fprintf(f, "<circle cx=\"%lf\" cy=\"%lf\" r=\"%lf\" stroke=\"black\" stroke-width=\"2\" fill=\"green\" />\n", mToPx(diszno.X), mToPx(magas-diszno.Y), mToPx(r));
    fprintf(f, "<rect x=\"%lf\" y=\"%lf\" stroke=\"black\" fill=\"brown\" width=\"%lf\" height=\"%lf\" />", mToPx(diszno.X-(r/2.0)), mToPx(magas-diszno.Y+r), mToPx(r), mToPx(diszno.Y));

    fprintf(f, "<circle cx=\"%lf\" cy=\"%lf\" r=\"%lf\" stroke=\"black\" stroke-width=\"2\" fill=\"red\" />\n", mToPx(red.X), mToPx(magas-red.Y), mToPx(r));
    fprintf(f, "<rect x=\"%lf\" y=\"%lf\" stroke=\"black\" fill=\"brown\" width=\"%lf\" height=\"%lf\" />\n", mToPx(red.X - (r / 2.0)), mToPx(magas - red.Y + r), mToPx(r), mToPx(red.Y));

    fprintf(f, "</svg>");
    fclose(f);
    return 0;
}

bool joPalya(palya p, position start, position stop){
    double alpha = (p.szog*PI)/180.0;
    double t = 0;
    double x = 0;
    double y = 0;

    while (x <= stop.X) {
        x = start.X + p.seb*t*cos(alpha);
        y = start.Y + p.seb*t*sin(alpha)-(g/2.0)*t*t;

        if (sqrt(pow(x-stop.X,2)+pow(y-stop.Y,2))<=r){
            return true;
        }

        t += tszamol;
    }
    return false;
}

void palyaRajz(FILE *f, palya p, position start, double stop){
    double t = 0;
    double x = start.X;
    double y = 0;
    double seb = p.seb;
    double alpha = (p.szog*PI)/180.0;
    while (x <= stop) {
        x = start.X + seb*t*cos(alpha);
        y = start.Y + seb*t*sin(alpha)-(g/2.0)*t*t;

        fprintf(f, "<circle cx=\"%lf\" cy=\"%lf\" r=\"%lf\" stroke=\"black\" stroke-width=\"1\" fill=\"white\" />\n", mToPx(x), mToPx(magas-y), mToPx(r/2));

        t += trajzol;
    }
}
#include <stdio.h>

int saveChar(){
    char ch;
    int in = scanf("%c", &ch);
    if (in == 0)
        ch = -1;
    return ch;
}

void run(char *code){
    int szalag[32768] = {0};
    int szalagpos = 0;

    int jump = 0;
    int jumps[10] = {0};
    int skip = 0;

    int pos = 0;
    char c = code[pos++];
    while (c != '\0'){
        if (skip <= 0){
            switch (c){
                case '.':
                    printf("%c", szalag[szalagpos]);
                    break;
                case ',':
                    szalag[szalagpos]=saveChar();
                    break;
                case '+':
                    szalag[szalagpos]++;
                    break;
                case '-':
                    szalag[szalagpos]--;
                    break;
                case '>':
                    szalagpos++;
                    break;
                case '<':
                    szalagpos--;
                    break;
                case '[':
                    if (szalag[szalagpos]){
                        jumps[++jump] = pos;
                    } else {
                        skip = 1;
                    }
                    break;
                case ']':
                    pos = jumps[jump--]-1;
                    if (pos == 0){
                        printf("Hibas BF program");
                        return;
                    }
                    break;
                default:
                    break;
            }
        } else {
            switch (c){
                case ']':
                    skip--;
                    break;
                case '[':
                    skip++;
                    break;
                default:
                    break;
            }
        }

        c = code[pos++];
    }
}

int main() {
    run("++++++++[>++++++++<-]>++.++++."); // BF
    printf("\n");
    run("[ThisprogramprintsSierpinskitriangleon80-columndisplay.]>++++[<++++++++>-]>++++++++[>++++<-]>>++>>>+>>>+<<<<<<<<<<[-[->+<]>[-<+>>>.<<]>>>[[->++++++++[>++++<-]>.<<[->+<]+>[->++++++++++<<+>]>.[-]>]]+<<<[-[->+<]+>[-<+>>>-[->+<]++>[-<->]<<<]<<<<]++++++++++.+++.[-]<]+++++*****Made*By:*NYYRIKKI*2002*****"); //Sierpinsky
    printf("\n");
    run("+++++++++[>++++++++<-]>+++++.++++++++++++++++++++++++++++.++.--."); // Miki
    printf("\n");
    run("]["); // Hibas BF progi

    return 0;
}
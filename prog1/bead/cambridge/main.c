// De így nem lesz meg a Speedy Gonzales
// https://www.youtube.com/watch?v=nkUOACGtGfA
// 😢😢😢

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

void fisherYates(char *word, int size){
    char temp;
    for (int i = size-1; i > 1 ; --i) {
        int j = rand() % i;
        temp = word[i];
        word[i] = word[j];
        word[j] = temp;
    }
}

void wordSort(char *word) {
    int shuffleSize = 0;

    while (!ispunct(word[shuffleSize]) && word[shuffleSize] != '\0'){
        shuffleSize++;
    }

    fisherYates(word + 1, shuffleSize - 2);
}

int main() {
    srand(time(0));

    char word[51] = {0};

    while (scanf("%s", word) == 1) {
        wordSort(word);
        printf("%s ", word);
    }

    return 0;
}
#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <stdlib.h>
#include <stdbool.h>

#define eps 3.0
#define mouseEps 10.0
#define sugar 3.0

typedef struct {
    int x, y;
} pont;

double tavolsag(pont p1, pont p2){
    return SDL_sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y));
}

typedef struct ellipszis{
    pont egyik;
    pont masik;
    double tavolsag;
}ellipszis;

bool ellipszisben(ellipszis e, pont j){
    return SDL_fabs(tavolsag(j, e.egyik)+tavolsag(j, e.masik)-e.tavolsag)<eps;
}

void rajzol(SDL_Renderer* renderer, ellipszis e){
    SDL_SetRenderDrawColor(renderer, 0, 0,  0, 255);
    SDL_RenderClear(renderer);
    pont j;
    for (j.x = 0; j.x < 640; j.x++){
        for (j.y = 0; j.y < 480; j.y++) {
            if (ellipszisben(e, j)){
                SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
                SDL_RenderDrawPoint(renderer, j.x, j.y);
            }
            if (SDL_fabs(tavolsag(j, e.egyik)-sugar) < eps || SDL_fabs(tavolsag(j, e.masik)-sugar) < eps){
                SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
                SDL_RenderDrawPoint(renderer, j.x, j.y);
            }
        }
    }
    char temp[20] = {0};
    sprintf(temp, "egyik=(%d,%d)", e.egyik.x, e.egyik.y);
    stringRGBA(renderer, 0, 450, temp, 255, 0, 0, 255);
    sprintf(temp, "masik=(%d,%d)", e.masik.x, e.masik.y);
    stringRGBA(renderer, 0, 460, temp, 255, 0, 0, 255);
    sprintf(temp, "tavolsag=%lf", e.tavolsag);
    stringRGBA(renderer, 0, 470, temp, 0, 255, 0, 255);
    SDL_RenderPresent(renderer);
}

int main(int argc, char *argv[]) {

    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        SDL_Log("Nem indithato az SDL: %s", SDL_GetError());
        exit(1);
    }
    SDL_Window *window = SDL_CreateWindow("Ellipszis", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, 0);
    if (window == NULL) {
        SDL_Log("Nem hozhato letre az ablak: %s", SDL_GetError());
        exit(1);
    }
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        SDL_Log("Nem hozhato letre a megjelenito: %s", SDL_GetError());
        exit(1);
    }
    SDL_RenderClear(renderer);
    SDL_Delay(100);
    
    ellipszis e = {
            .egyik = (pont){300, 300},
            .masik = (pont){350, 350},
            .tavolsag = 100
    };
    
    enum allapot{alap, huzTav, huzEgyik, huzMasik} allapot = alap;

    rajzol(renderer, e);
    
    stringRGBA(renderer, 10, 125, "Ellipszis", 0, 255, 0, 255);

    SDL_Event ev;
    pont eger = {0, 0};
    while (SDL_WaitEvent(&ev) && ev.type != SDL_QUIT) {
        if (ev.type == SDL_MOUSEBUTTONUP)
            allapot = alap;
        switch (allapot){
            case alap:
                if (ev.type == SDL_MOUSEBUTTONDOWN){
                    eger.x = ev.button.x;
                    eger.y = ev.button.y;
                    if (tavolsag(eger, e.egyik) < mouseEps){
                        allapot = huzEgyik;
                    } else if (tavolsag(eger, e.masik) < mouseEps) {
                        allapot = huzMasik;
                    } else if (ellipszisben(e, eger)) {
                        allapot = huzTav;
                    }
                }
                break;
            case huzTav:
                if (ev.type == SDL_MOUSEMOTION){
                    eger.x = ev.motion.x;
                    eger.y = ev.motion.y;
                }
                e.tavolsag = tavolsag(eger, e.egyik) + tavolsag(eger, e.masik);
                rajzol(renderer, e);
                break;
            case huzEgyik:
                if (ev.type == SDL_MOUSEMOTION){
                    eger.x = ev.motion.x;
                    eger.y = ev.motion.y;
                }
                e.egyik = eger;
                rajzol(renderer, e);
                break;
            case huzMasik:
                if (ev.type == SDL_MOUSEMOTION){
                    eger.x = ev.motion.x;
                    eger.y = ev.motion.y;
                }
                e.masik = eger;
                rajzol(renderer, e);
                break;
            default:
                break;
        }
    }

    SDL_Quit();

    return 0;
}
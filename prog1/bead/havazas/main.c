#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
//#import "debugmalloc.h" // 😍

#define radius 4
#define fallspeed 2

typedef struct {
    int x, y;
} hopihe;

int piheszam = 0;

double tavolsag(hopihe p1, hopihe p2){
    return sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y));
}

typedef struct pihelista {
    hopihe hely;
    struct pihelista* next;
} pihelista;

void newPihe(pihelista** pihe){
    pihelista* uj = malloc(sizeof(pihelista));
    uj->next = *pihe;
    uj->hely = (hopihe){rand() % 800, 0};
    *pihe = uj;
}

Uint32 timer(Uint32 interval, void* params){
    pihelista** pihe = (pihelista**) params;
    newPihe(pihe);
    piheszam++;
    return interval;
}

void listaSzab(pihelista** lista){
    pihelista* lemarad = NULL;
    pihelista* mozgo = *lista;
    while (mozgo != NULL) {
        free(lemarad);
        lemarad = mozgo;
        mozgo = mozgo->next;
    }
    free(lemarad);
    *lista = NULL;
}

bool utkoz(pihelista* hely, pihelista* pihek){
    for (pihelista* mozgo = pihek; mozgo != NULL; mozgo = mozgo->next) {
        if (hely != mozgo && hely->hely.y <= mozgo->hely.y && tavolsag(hely->hely, mozgo->hely) < radius)
            return true;
    }
    return false;
}

Uint32 mover(Uint32 interval, void* params){
    pihelista** pihe = (pihelista**) params;
    for (pihelista* mozgo = *pihe; mozgo != NULL; mozgo = mozgo->next) {
        if (mozgo->hely.y<600 && !utkoz(mozgo, *pihe)){
            mozgo->hely.y+=fallspeed;
            mozgo->hely.x+=rand()%3-1;
        }
    }
    SDL_Event ev;
    ev.type = SDL_USEREVENT;
    SDL_PushEvent(&ev);
    return interval;
}

int main(int argc, char *argv[]) {
    srand(time(NULL));

    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        SDL_Log("Nem indithato az SDL: %s", SDL_GetError());
        exit(1);
    }
    SDL_Window *window = SDL_CreateWindow("Hull a hó és hózik", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, 0);
    if (window == NULL) {
        SDL_Log("Nem hozhato letre az ablak: %s", SDL_GetError());
        exit(1);
    }
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        SDL_Log("Nem hozhato letre a megjelenito: %s", SDL_GetError());
        exit(1);
    }
    SDL_RenderClear(renderer);

    pihelista* pihe = NULL;
    
    SDL_TimerID id = SDL_AddTimer(1000/10, timer, &pihe);
    SDL_TimerID i2 = SDL_AddTimer(1000/60, mover, &pihe);
    
    SDL_Event ev;
    char pihecnt[20] = {0};
    
    while (SDL_WaitEvent(&ev) && ev.type != SDL_QUIT) {
        if(ev.type == SDL_USEREVENT){
            sprintf(pihecnt, "%d", piheszam);
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
            SDL_RenderClear(renderer);
    
            for (pihelista* mozgo = pihe; mozgo != NULL ; mozgo = mozgo->next) {
                filledCircleRGBA(renderer, mozgo->hely.x, mozgo->hely.y, radius, 255, 255, 255, 255);
            }
    
            stringRGBA(renderer, 10, 10, pihecnt, 255, 255, 255, 255);
    
            SDL_RenderPresent(renderer);
        }
    }

    SDL_RemoveTimer(id);
    SDL_RemoveTimer(i2);
    SDL_Quit();

    listaSzab(&pihe);
    return 0;
}
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define szintszam 8

struct foglalas {
    int szoba;
    char *nev;
    struct foglalas *kovetkezo;
};

void foglalasKiir(struct foglalas *root) {
    struct foglalas *current;
    current = root;
    if (current != 0) {
        printf("%d %s\n", current->szoba, current->nev);
        while (current->kovetkezo != 0) {
            current = current->kovetkezo;
            printf("%d %s\n", current->szoba, current->nev);
        }
    }
}

void ujFoglalas(struct foglalas *root, int szoba, char* nev) {
    if (root->kovetkezo == 0 && root->nev == 0){
        root->nev = nev;
        root->szoba = szoba;
        root->kovetkezo = 0;
        return;
    }
    struct foglalas *current;
    current = root;
    while (current->kovetkezo != 0) {
        current = current->kovetkezo;
    }
    current->kovetkezo = malloc(sizeof(struct foglalas));
    current = current->kovetkezo;
    current->kovetkezo = 0;
    current->nev = nev;
    current->szoba = szoba;
}

struct foglalas *foglalasBeolvas(char* fileNev) {
    struct foglalas *root = malloc(sizeof(struct foglalas));
    root->kovetkezo = 0;

    FILE* f = fopen(fileNev, "r");

    char line[200] = "";
    while (fgets(line, sizeof(line), f)){
        int szoba = 0;

        char *nev = malloc(sizeof(char[100]));
        /* nem tudok itt statikus tömböt használni (szerintem),
         * mivel az az inicializáló string helyére mutatna,
         * így a lista összes eleme egy helyre mutatna,
         * malloc pedig friss memóriára mutat,
         * de lehet van valami C mágia, amivel statikus tömb nem ugyanoda mutat 🤷‍♂️ */

        sscanf(line, "%d %[^\n]", &szoba, nev);
        ujFoglalas(root, szoba, nev);
    }
    return root;
}

void foglalasSzintenkent(struct foglalas *root, int *szintek) {
    for (unsigned long int i = 0; i < szintszam; i++){
        szintek[i] = 0;
    }
    struct foglalas *current = root;
    szintek[current->szoba/100]++;
    while (current->kovetkezo != 0) {
        current = current->kovetkezo;
        szintek[current->szoba/100]++;
    }
}

int zsufolt(int *szintek) {
    int max = 0;
    int maxhely = 0;
    for (int i = 0; i < szintszam; i++){
        if (max<szintek[i]) {
            max = szintek[i];
            maxhely = i;
        }
    }
    return maxhely;
}

struct foglalas *keres(struct foglalas *root, const char *nev){
    struct foglalas *current = root;
    if (strcmp(current->nev, nev) == 0){
        return current;
    } else {
        while (current->kovetkezo != 0) {
            current = current->kovetkezo;
            if (strncmp(current->nev, nev, strnlen(nev, 1000)) == 0){
                return current;
            }
        }
    }
    return 0;
}

void utolsoFeladat(char *filenev, char *nev) {
    struct foglalas *foglalasok = foglalasBeolvas(filenev);
    struct foglalas *keresett = keres(foglalasok, nev);
    if(keresett == 0){
        printf("%s nem szállt meg a szállodában.\n", nev);
    } else {
        printf("%s megszállt a szállodában és szobája (%d) ", nev, keresett->szoba);
        int szintek[szintszam];
        foglalasSzintenkent(foglalasok, szintek);
        if (zsufolt(szintek) != keresett->szoba/100)
            printf("nem ");
        printf("a legzsúfoltabb szinten volt.");
    }
}

int main() {
    struct foglalas *foglalasok = foglalasBeolvas("./test.txt");
    foglalasKiir(foglalasok);

    int szintek[szintszam];
    foglalasSzintenkent(foglalasok, szintek);
    printf("legzsúfoltabb szint: %d\n", zsufolt(szintek));

    char *nev = "Tóth Róza";
    struct foglalas *roza = keres(foglalasok, nev);
    printf("%s %d\n", roza->nev, roza->szoba);

    utolsoFeladat("./test.txt", "Horváth Béla");
}
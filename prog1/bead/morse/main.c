#include <stdio.h>
#include <stdlib.h>
//#include "debugmalloc.h"

#define indent 4

typedef struct faelem{
    char betu;
    struct faelem* rovid;
    struct faelem* hosszu;
}faelem;

faelem* new_node(){
    faelem* node = malloc(sizeof(faelem));
    node->betu = ' ';
    node->rovid = node->hosszu = NULL;
    return node;
}

void append(faelem* root, char letter, char* morse){
    faelem* moving = root;
    for (int i = 0; morse[i] != '\0'; ++i) {
        if (morse[i] == '.'){
            if (moving->rovid == NULL)
                moving->rovid = new_node();
            moving = moving->rovid;
        } else if (morse[i] == '-') {
            if (moving->hosszu == NULL)
                moving->hosszu = new_node();
            moving = moving->hosszu;
        } else {
            perror("Hibás morse kód!\n");
            return;
        }
    }
    moving->betu = letter;
}

void indentMePls(FILE* f, int inDepth){
    for (int i = 0; i < indent*(inDepth+1); ++i) {
        fprintf(f, " ");
    }
}

void dekodEpit(FILE* f, faelem* root, int depth){
    if (root == NULL)
        return;
    
    if (root->rovid != NULL){
        indentMePls(f, depth);
        fprintf(f, "if(kod[%d] == '.')\n", depth);
        dekodEpit(f, root->rovid, depth+1);
    }
    if (root->hosszu != NULL){
        indentMePls(f, depth);
        if (root->rovid != NULL){
            fprintf(f, "else ");
        }
        fprintf(f, "if(kod[%d] == '-')\n", depth);
        dekodEpit(f, root->hosszu, depth+1);
    }
    indentMePls(f, depth);
    if (root->rovid != NULL || root->hosszu != NULL){
        fprintf(f, "else\n");
        indentMePls(f, depth+1);
    }
    fprintf(f, "printf(\"%%c\", '%c');\n", root->betu);
}

void freeTree(faelem* node){
    if (node == NULL)
        return;
    freeTree(node->hosszu);
    freeTree(node->rovid);
    free(node);
}

int main() {
    char tomb[][10] = {
            "A", ".-",
            "B", "-...",
            "C", "-.-.",
            "D", "-..",
            "E", ".",
            "F", "..-.",
            "G", "--.",
            "H", "....",
            "I", "..",
            "J", ".---",
            "K", "-.-",
            "L", ".-..",
            "M", "--",
            "N", "-.",
            "O", "---",
            "P", ".--.",
            "Q", "--.-",
            "R", ".-.",
            "S", "...",
            "T", "-",
            "U", "..-",
            "V", "...-",
            "W", ".--",
            "X", "-..-",
            "Y", "-.--",
            "Z", "--..",
            "1", ".----",
            "2", "..---",
            "3", "...--",
            "4", "....-",
            "5", ".....",
            "6", "-....",
            "7", "--...",
            "8", "---..",
            "9", "----.",
            "0", "-----",
            ""};
    faelem* root = new_node();
    for (int i = 0; tomb[i][0] != '\0' ; i+=2) {
        append(root, tomb[i][0], tomb[i+1]);
    }
    
    FILE* f = fopen("morzedekoder.c", "w");
    
    fprintf(f, "#include <stdio.h>\n\nvoid dekod(char* kod){\n");
    dekodEpit(f, root, 0);
    fprintf(f, "}\n\nint main(){\n");
    
    indentMePls(f, 1);
    fprintf(f, "dekod(\"--\");\n");
    indentMePls(f, 1);
    fprintf(f, "dekod(\"..\");\n");
    indentMePls(f, 1);
    fprintf(f, "dekod(\"-.-\");\n");
    indentMePls(f, 1);
    fprintf(f, "dekod(\"..\");\n");
    indentMePls(f, 1);
    fprintf(f, "printf(\"\\n\");\n");
    indentMePls(f, 1);
    fprintf(f, "return 0;\n");
    fprintf(f, "}");
    
    fclose(f);
    freeTree(root);
    return 0;
}
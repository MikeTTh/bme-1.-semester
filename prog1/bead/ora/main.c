#include <stdio.h>
#include <math.h>

#define size 200

void drawLine(FILE* f, double angle, double start, double end){
    double x = sin(angle);
    double y = cos(angle);

    double offset = (double)size/2;

    fprintf(f, "<line x1=\"%f\" x2=\"%f\" y1=\"%f\" y2=\"%f\" stroke=\"black\" />", x*start+offset, x*end+offset, y*start+offset, y*end+offset);
}

int main() {
    double pi = acos(-1);
    int h, m, s;
    scanf("%d %d %d", &h, &m, &s);

    FILE* f = fopen("./ora.svg", "w");

    fprintf(f, "<svg width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">", size, size);
    fprintf(f, "<circle cx=\"%d\" cy=\"%d\" r=\"%d\" stroke=\"black\" fill=\"white\" />", size/2, size/2, size/2);
    fprintf(f, "<text x=\"50%%\" y=\"80%%\" text-anchor=\"middle\"> Rollex </text>");


    for (int i = 0; i < 12; ++i) {
        drawLine(f, i*pi/6.0, (double)size/20*9, (double)size/2);
    }
    for (int i = 0; i < 60; ++i) {
        drawLine(f, i*pi/30.0, (double)size/40*19, (double)size/2);
    }

    drawLine(f, -(h*pi/6.0)+pi-(m*pi/30.0)/12-(s*pi/30.0)/60, 0, (double)size/3);
    drawLine(f, -(m*pi/30.0)+pi-(s*pi/30.0)/60, 0, (double)size/2.5);
    drawLine(f, -(s*pi/30.0)+pi, 0, (double)size/2);

    fprintf(f, "</svg>");
    fclose(f);
    return 0;
}

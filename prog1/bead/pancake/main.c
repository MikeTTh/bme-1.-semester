#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct list {
    int data;
    struct list* next;
    struct list* prev;
} list;

void beszur(list** l, int data){
    list *newList = malloc(sizeof(list));
    newList->data = data;
    newList->next = *l;
    newList->prev = NULL;
    if (*l != NULL)
        (*l)->prev=newList;
    *l = newList;
}

void kiir(list* l, list* serpenyo){
    for (list* mov = l; mov != NULL ; mov = mov->next) {
        printf("%2d%c", mov->data, mov == serpenyo?'|':' ');
    }
    printf("\n");
}

void fordit(list* l, list* strazsa){
    kiir(strazsa->next, l);
    list* temp = l->next;
    list* mozg = l;
    list* lemarad = strazsa;
    while (mozg != NULL){
        mozg->next = mozg->prev;
        mozg->prev = lemarad;
        lemarad = mozg;
        mozg = mozg->next;
    }
    if (temp != NULL)
        temp->prev = strazsa->prev;
    strazsa->prev->next = temp;
    strazsa->next = l;
    strazsa->prev = NULL;
}

void pancake(list* strazsa) {
    if (strazsa == NULL || strazsa->next == NULL)
        return;
    
    list* rendezett = NULL;
    
    list* min = strazsa->next;
    for (list* mozgo = strazsa->next; mozgo != rendezett; mozgo=mozgo->next){
        if (mozgo->data < min->data){
            min = mozgo;
        }
    }
    
    while (true){
        list* max = strazsa->next;
        for (list* mozgo = strazsa->next; mozgo != rendezett; mozgo=mozgo->next){
            if (mozgo->data > max->data){
                max = mozgo;
            }
        }
        if(max == min){
            break;
        }
        fordit(max, strazsa);
        list* utolso = NULL;
        for (utolso = strazsa->next; utolso->next != rendezett; utolso = utolso->next);
        fordit(utolso, strazsa);
        rendezett = max;
    }
}

int main() {
    int szamok[10] = {3, 5, 2, 1, 4, 9, 8, 10, 6, 7};
    list* l = NULL;
    for (int i = 0; i < 10; ++i) {
        beszur(&l, szamok[i]);
    }
    beszur(&l, -1); // strázsa
    pancake(l);
    kiir(l->next, NULL);
    return 0;
}

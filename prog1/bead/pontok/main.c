#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <math.h>
#include <stdlib.h>

typedef struct {
    int x, y;
} pont;

double tavolsag(pont p1, pont p2){
    return sqrt(pow((double)p1.x-(double)p2.x, 2)+pow((double)p1.y-(double)p2.y, 2));
}

int main(int argc, char *argv[]) {
    double eps = 3;

    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        SDL_Log("Nem indithato az SDL: %s", SDL_GetError());
        exit(1);
    }
    SDL_Window *window = SDL_CreateWindow("Azon pontok mértani helye...", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, 0);
    if (window == NULL) {
        SDL_Log("Nem hozhato letre az ablak: %s", SDL_GetError());
        exit(1);
    }
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        SDL_Log("Nem hozhato letre a megjelenito: %s", SDL_GetError());
        exit(1);
    }
    SDL_RenderClear(renderer);

    pont j;

    for (j.x = 0; j.x < 640; j.x++){
        for (j.y = 0; j.y < 480; j.y++) {
            if (fabs((tavolsag(j, (pont){320, 240})-200.0))<eps){
                SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
                SDL_RenderDrawPoint(renderer, j.x, j.y);
            }
            if (fabs(tavolsag(j, (pont){240, 200})+tavolsag(j, (pont){400, 280})-250.0)<eps){
                SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
                SDL_RenderDrawPoint(renderer, j.x, j.y);
            }
            if (fabs(fabs(tavolsag(j, (pont){240, 240})-tavolsag(j, (pont){400, 240}))-100.0)<eps){
                SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
                SDL_RenderDrawPoint(renderer, j.x, j.y);
            }
            if (fabs(tavolsag(j, (pont){320, 240}) - fabs(j.x-400.0))<eps){
                SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
                SDL_RenderDrawPoint(renderer, j.x, j.y);
            }
        }
    }

    stringRGBA(renderer, 10, 100, "Kor", 255, 0, 0, 255);
    stringRGBA(renderer, 10, 125, "Ellipszis", 0, 255, 0, 255);
    stringRGBA(renderer, 10, 150, "Hiperbola", 0, 0, 255, 255);
    stringRGBA(renderer, 10, 175, "Parabola", 255, 255, 255, 255);

    SDL_RenderPresent(renderer);

    SDL_Event ev;
    while (SDL_WaitEvent(&ev) && ev.type != SDL_QUIT) {
    }

    SDL_Quit();

    return 0;
}
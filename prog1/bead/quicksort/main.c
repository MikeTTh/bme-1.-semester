#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include "debugmalloc.h"

typedef struct Node{
    char word[100];
    struct Node* next;
} Node;

Node* initList(){
    char szavak[][100]={"b", "bbbbbb", "abababa", "aaaaab", "aaaaa", "cccccc", "zzzzzzzz", "ddddddd", "zzzzzzzzzz", ""};
    Node* start = malloc(sizeof(Node));
    Node* mozgo = start;
    for (int i = 0; szavak[i][0] != '\0' ; ++i) {
        strcpy(mozgo->word, szavak[i]);
        if (szavak[i+1][0] != '\0'){
            mozgo->next = malloc(sizeof(Node));
            mozgo = mozgo->next;
        }
    }
    mozgo->next = NULL;
    return start;
}

Node* hozzafuz(Node* lista, Node* elem){
    elem->next = NULL;
    if (lista == NULL){
        return elem;
    } else {
        Node* mozgo = lista;
        while (mozgo->next != NULL)
            mozgo = mozgo->next;
        mozgo->next = elem;
        return lista;
    }
}

Node* osszefuz(Node* egyik, Node* masik){
    if(egyik == NULL)
        return masik;
    Node* mozgo = egyik;
    while (mozgo->next != NULL)
        mozgo = mozgo->next;
    mozgo->next = masik;
    return egyik;
}

Node* sort(Node* list){
    // Rekurzió vége, ha nincs lista (Hibás függvényhívás), vagy 1 elemű a lista
    if(list == NULL || list->next == NULL)
        return list;
    
    // A részlisták első elemei
    Node *kicsi = NULL, *egyenlo = NULL, *nagy = NULL;
    Node* mozgo = list;
    
    while (mozgo != NULL){
        // A vezérelemnek az első elemet használom
        Node* temp = mozgo;
        mozgo = mozgo->next;
        
        if (strcmp(temp->word, list->word) == 0){
            egyenlo = hozzafuz(egyenlo, temp);
        }
        else if (strcmp(temp->word, list->word) < 0){
            kicsi = hozzafuz(kicsi, temp);
        }
        else if (strcmp(temp->word, list->word) > 0){
            nagy = hozzafuz(nagy, temp);
        }
    }
    
    kicsi = sort(kicsi);
    nagy = sort(nagy);
    
    Node* rendezett = osszefuz(kicsi, egyenlo);
    rendezett = osszefuz(rendezett, nagy);
    
    return rendezett;
}

void printList(Node* list){
    while(list != NULL){
        printf("%s\n", list->word);
        list = list->next;
    }
}

void freeList(Node* list){
    Node* next = list;
    while (next != NULL){
        next = list->next;
        free(list);
        list = next;
    }
}

int main() {
    Node* list = initList();
    printList(list);
    
    printf("\n");
    list = sort(list);
    
    printList(list);
    
    freeList(list);
    
    return 0;
}
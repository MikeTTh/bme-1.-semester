#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* sorolv(int hossz){
    char c;
    if (scanf("%c", &c) == EOF || c == '\n'){
        char* string = malloc((hossz+1) * sizeof(char));
        string[hossz] = '\0';
        return string;
    } else {
        char* string = sorolv(hossz+1);
        string[hossz] = c;
        return string;
    }
}

char* sort_beolvas(void){
    return sorolv(0);
}

int main() {
    char* string = sort_beolvas();
    printf("%s", string);
    free(string);
    return 0;
}
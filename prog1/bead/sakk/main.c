#include <stdio.h>

int main() {
    int ismet = 1;
    scanf("%d", &ismet);

    for (int y = 0; y<8; y++) {
        for (int j = 0; j < ismet; ++j) {
            for (int x = 0; x<8; x++) {
                int cellaparitas = x;
                if (y%2==0)
                    cellaparitas ++;

                char printed = 'X';
                if (cellaparitas % 2 == 0)
                    printed = '.';

                for (int i = 0; i < ismet; ++i) {
                    printf("%c", printed);
                }
            }
            printf("\n");
        }
    }

    return 0;
}

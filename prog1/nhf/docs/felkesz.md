# Programozás alapjai nagy házi feladat, félkész megoldás

## Program struktúra

![Program struktúra](structure.svg)

### `snake_common.h`

A `snake_common.h` tartalmazza a program azon részeit amit az összes többi
modul használ. Itt található például a kígyókat leíró listaelemek típúsa is.

### `logic.c`

A `logic.c` tartalmazza az összes játékmenethez tartózó függvényt, 
mint például a kígyót mozgató függvényt.

### `rajz.c`

A `rajz.c` tartalmazza az összes rajzoláshoz és SDL-kezeléshez tartozó függvényt az event loop kivételével.

### `main.c`

A `main.c` tartalmazza az event loop-ot és ez a program kiindulópontja.

## Fordítás

A programhoz mellékelve van a `build.sh` shell script, amely a program fordításáért felel és megfelelően
építi és linkeli a modulokat.

## Működő elemek, használat 

A félkész programban már működnek a következő részek:
* kígyók rajzolása
* kígyók mozgatása
* irányítás
* vsync, vagyis a monitor frissítési sebességével fut a játék 
* étel felszedése
* étel pozíció generálása

Azonban hiányoznak a következők:
* kígyók halálának észlelése \(jelenleg félkész állapotban `bool checkdeath()` függvényben \)
* pályák betöltése
* főmenü
* játékmódok, azok végének észlelése

A program lefordított változatai építés után a `build` mappában találhatóak, Linux-on a `build/snake`, Windows-on a `build\snake.exe` futtatható.

## Screenshot

![Screenshot](Screenshot.png)

## Ismert hiányosságok

* amennyiben az fps változik, megjelenhetnek különböző rajzolási pontatlanságok
* a bezáráskor még egy rajzolási hívás történik, emiatt a program `SIGSEGV`-vel zárul
* a kígyók tárolása jelenleg nem a leghatékonyabb módon történik, felesleges listaelemek találhatóak a kígyók végén
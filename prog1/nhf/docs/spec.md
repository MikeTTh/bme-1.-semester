# Programozás alapjai NHF specifikáció


## Feladat

A *Kígyó játék* feladatot választottam a nagy házi listából.
Ez alatt egy olyan játékot értünk, amelyben egy kígyó a felhasználótól bekért irányba halad 
és egy 2 dimenziós pályán lévő ételhez érve 1 egység hosszal nő.

## Technikai háttér

A játék az SDL könyvtár segítségével fog rajzolni a képernyőre.
A pályákat szöveges fájlokból lehet beolvasni.

## A pálya

A pályát leíró szöveges fájlban egy X az olyan 1\*1 egységni területet (ahol a kígyó feje és további elemei is mind 1\*1 egység méretűek) jelent, amibe beleütközve meghal a kígyó,
az S az a kígyó kiindulási helyét jelöli, minden más karakter olyan helyet jelent, ahol lehet mozogni.
Példaul ez a pálya:
```
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

  S

            XXXX
            XXXX



XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```
egy 30\*10 egység méretű pályát eredményezne, amelyben a kígyó a (3, 3) helyen kezd és a pálya alja és teteje közt nem lehet átmenni, illetve a közepén található egy 4\*2 egység méretű tiltott hely.

## Multiplayer

A játékban található sigle- és multiplayer mód is, vagyis egyedül vagy több játékossal is játszható.
Ezek a módok közül a főmenüben lehet választani.

## Cél

A játékban 2 féle játékmenet közül lehet választani:  

* normál mód
    * egy bizonyos pontszám elérésével lehet nyerni
    * aki előbb eléri, az nyerstringRGBA(render, 10, 100, "Hello", 255, 0, 0, 255);
* végtelen mód
    * a kígyó haláláig megy a játék
    * aki utolsónak életben marad, az nyer


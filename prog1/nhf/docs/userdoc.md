# Sznék felhasználói dokumentáció
Programozás alapjai I.  
Tóth Miklós, FLGIIG

## Főmenü
A játék megnyitása után a főmenüben találjuk magunkat.  
![Főmenü](menu.png)  

Itt az egérrel kattintva választhatunk a játékmódok közül:   

   * egyjátékos vagy kétjátékos: választhatunk 1 vagy 2 játékos közt   
   
   * normál vagy végtelen: normál módban egy maximum pontszámot elérve is lehet nyerni, míg végtelenben a pontszám nem számít a nyerés szempontjából
      
      

A játék a *START* gombra kattintva indul.

## Játékmenet
Egyjátékos mód:  
![single](single.png)

Kétjátékos mód:  
![multi](2player.png)  

A bal oldalon látható az első játékos pontszáma.
Többjátékos módban a jobb oldalon látható a második játékos pontszáma.
Normál módban a nyeréshez elegendő pontszám középen látszik.
Az első játékos sárga színű és a W, A, S, D gombokkal irányítható,
a második játékos (amennyiben van) zöld színű és a nyilakkal irányítható.

## A játék vége

### Egyjátékos
Nyerés:  
![win](win.png)
  
Vesztés:  
![lose](lose.png)  

A játék végén normál módban nyerni vagy veszteni lehet, végtelen módban csak veszteni.

### Kétjátékos
1. játékos nyer  
![1stwin](1stwin.png)
  
2. játékos nyer  
![2ndwin](2ndwin.png)  

A kétjátékos mód végén mindenképpen az egyik játékos fog nyerni.
A nyertes az a játékos lesz, amelyik nem halt meg (ha a másik meghalt)
vagy normál mód esetén az, aki előbb eléri a nyeréshez elegendő pontot.

Ebben az állapotban az új játék gombra kattintva visszakerülünk a *Főmenü*be, a bezárás gombra kattintva pedig bezárul a játék.
//
// Created by mike on 29/11/2019.
//

#include "eventloops.h"
#include "snake_common.h"
#include "rajzol.h"
#include "logic.h"
#include <SDL2/SDL.h>
#include "debugmalloc.h"


void initLoop(gameStruct* data){
    position mouse = {-1, -1};
    while(SDL_PollEvent(&data->event)){ // Az utolsó rajzolás alatt történt event közül a legfrissebb kattintást mentem
        switch (data->event.type){
            case SDL_MOUSEBUTTONDOWN:
                mouse = (position){data->event.button.x, data->event.button.y};
                break;
            default:
                break;
        }
    }
    if (rajzKezd(data->renderer, &data->gm, data->font, mouse)){ // Ha start volt, akkor inicializálunk a játékmódnak megfelelően
        createSnake(0, data);
        if (data->gm.multiplayer)
            createSnake(1, data);
        newFood(data);
    
        data->playerTimers[0] = SDL_AddTimer(1000 / sampletime, snakeMover, data->players[0]);
        if(data->gm.multiplayer)
            data->playerTimers[1] = SDL_AddTimer(1000 / sampletime, snakeMover, data->players[1]);
        data->state = game;
        return;
    }
}

void gameLoop(gameStruct* data){
    while (SDL_PollEvent(&data->event)) { // Egy rajzolás alatt történt gombnyomások feldolgozása
        switch (data->event.type) {
            case SDL_KEYDOWN:
                switch (data->event.key.keysym.scancode) {
                    case SDL_SCANCODE_W:
                        if (data->players[0]->direction.y != 1)
                            data->players[0]->direction = (position) {0, -1};
                        break;
                    case SDL_SCANCODE_S:
                        if (data->players[0]->direction.y != -1)
                            data->players[0]->direction = (position) {0, 1};
                        break;
                    case SDL_SCANCODE_A:
                        if (data->players[0]->direction.x != 1)
                            data->players[0]->direction = (position) {-1, 0};
                        break;
                    case SDL_SCANCODE_D:
                        if (data->players[0]->direction.x != -1)
                            data->players[0]->direction = (position) {1, 0};
                        break;
                    case SDL_SCANCODE_UP:
                        if (data->gm.multiplayer && data->players[1]->direction.y != 1)
                            data->players[1]->direction = (position) {0, -1};
                        break;
                    case SDL_SCANCODE_DOWN:
                        if (data->gm.multiplayer && data->players[1]->direction.y != -1)
                            data->players[1]->direction = (position) {0, 1};
                        break;
                    case SDL_SCANCODE_LEFT:
                        if (data->gm.multiplayer && data->players[1]->direction.x != 1)
                            data->players[1]->direction = (position) {-1, 0};
                        break;
                    case SDL_SCANCODE_RIGHT:
                        if (data->gm.multiplayer && data->players[1]->direction.x != -1)
                            data->players[1]->direction = (position) {1, 0};
                        break;
                    default:
                        break;
                }
                break;
        }
    }
    
    moveSnakes(data);
    checkFood(data);
    
    drawSnakes(data);
    
    drawPalya(data);
    drawFood(data);
    
    // Lehetséges ütközések kezelése
    if (checkDeath(data->players[0], data->players[0]->next) || (data->gm.multiplayer && checkDeath(data->players[0], data->players[1])) || checkWallHit(data->players[0], *data->p)) {
        data->winner = 1;
        data->state = win;
        return;
    }else if (data->gm.multiplayer && (checkDeath(data->players[1], data->players[1]->next) || checkDeath(data->players[1], data->players[0]) || checkWallHit(data->players[1], *data->p))) {
        data->winner = 0;
        data->state = win;
        return;
    }
    
    // A max pont elérésének ellenőrzése
    if(!data->gm.death){
        if (data->players[0]->score >= data->p->normalModeScore){
            data->winner = 0;
            data->state = win;
            return;
        }
        if (data->gm.multiplayer && data->players[1]->score >= data->p->normalModeScore){
            data->winner = 1;
            data->state = win;
            return;
        }
    }
    
    SDL_Rect p0ScorePos = {(int)data->unit, (int)data->unit, (int)data->unit, (int)data->unit};
    numDraw(data->renderer, data->font, data->players[0]->score, &p0ScorePos, data->players[0]->color);
    
    if (!data->gm.death){
        SDL_Rect winScore = {(int)((double)resX/2.0-data->unit/2.0), (int)data->unit, (int)data->unit, (int)data->unit};
        numDraw(data->renderer, data->font, data->p->normalModeScore, &winScore, (SDL_Color){255, 255, 255, 255});
    }
    
    if (data->gm.multiplayer){
        SDL_Rect p1ScorePos = {(int)((data->p->size.x-2)*data->unit), (int)data->unit, (int)data->unit, (int)data->unit};
        numDraw(data->renderer, data->font, data->players[1]->score, &p1ScorePos, data->players[1]->color);
    }
}

void restart(void* params){ // Felszabadítás és minimális újrainicializálás
    gameStruct* data = (gameStruct*)params;
    gameStruct d = initStruct(data->renderer, data->font, data->unit, palyaTolt("palya.txt"));
    felszabadit(data);
    *data = d;
    data->winner = -1;
    data->state = menu;
}
void bezar(void* params){
    gameStruct* data = (gameStruct*)params;
    data->state = done;
}

void endLoop(gameStruct* data){
    position mouse = {-1, -1};
    while (SDL_PollEvent(&data->event)) { // Az utolsó rajzolás alatt történt event közül a legfrissebb kattintást mentem
        switch (data->event.type) {
            case SDL_MOUSEBUTTONDOWN:
                mouse = (position) {data->event.button.x, data->event.button.y};
                break;
            default:
                break;
        }
    }
    
    int margin = resY / 10;
    
    SDL_Rect rect = {
            .x = margin,
            .h = resX / 8,
            .w = resX - margin*2,
            .y = margin
    };
    renderText(data->renderer, data->font, "Játék vége", &rect, (SDL_Color){0xFF, 0xFF, 0xFF, 0xFF});
    
    rect.y = resX/8 + margin;
    char* nyertes = NULL;
    if (data->gm.multiplayer){
        nyertes = malloc((strlen("A(z) x. játékos nyert!")+1)*sizeof(char));
        sprintf(nyertes, "A(z) %d. játékos nyert!", data->winner+1);
        renderText(data->renderer, data->font, nyertes, &rect, data->players[data->winner]->color);
    } else {
        if (data->winner == 1) {
            nyertes = malloc((strlen("Vesztettél.")+1)*sizeof(char));
            strcpy(nyertes, "Vesztettél.");
        } else {
            nyertes = malloc((strlen("Nyertél!")+1)*sizeof(char));
            strcpy(nyertes, "Nyertél!");
        }
        renderText(data->renderer, data->font, nyertes, &rect, data->players[0]->color);
    }
    free(nyertes);
    
    rect.y *= 2;
    rect.w /= 2;
    rect.w -= margin;
    buttonHandler(data->renderer, data->font, &rect, (SDL_Color){255, 255, 255, 255}, "Új Játék", mouse, restart, data);
    
    rect.x += 2*margin + rect.w;
    buttonHandler(data->renderer, data->font, &rect, (SDL_Color){255, 255, 255, 255}, "Bezárás", mouse, bezar, data);
    
}
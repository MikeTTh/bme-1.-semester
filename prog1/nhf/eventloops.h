//
// Created by mike on 29/11/2019.
//
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "snake_common.h"

#ifndef NHF_EVENTLOOPS_H
#define NHF_EVENTLOOPS_H



void initLoop(gameStruct* data);
void gameLoop(gameStruct* data);
void endLoop(gameStruct* data);

#endif //NHF_EVENTLOOPS_H

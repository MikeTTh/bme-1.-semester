//
// Created by mike on 06/11/2019.
//

#include <stdlib.h>
#include "logic.h"
#include "snake_common.h"
#include <stdbool.h>
#include <stdio.h>
#include <SDL2/SDL.h>
#include "debugmalloc.h"



SDL_Color valid[] = {{0xFC, 0xBA, 0x03, 255}, {0x32, 0xA8, 0x52, 255}};

position loopAround(position pos, position max){ // Pálya körkörösítése
    if(pos.x > max.x-1)
        pos.x = 0;
    if(pos.y > max.y-1)
        pos.y = 0;
    if(pos.x < 0)
        pos.x = max.x-1;
    if(pos.y < 0)
        pos.y = max.y-1;
    return pos;
}

void moveSnakes(gameStruct* data){
    for (int i = 0; i<2 && data->players[i] != NULL ; ++i) {
        data->players[i]->pos.x += data->players[i]->direction.x*(speed * (double)data->lastTick / 1000.0);
        data->players[i]->pos.y += data->players[i]->direction.y*(speed * (double)data->lastTick / 1000.0);
        data->players[i]->pos = loopAround(data->players[i]->pos, data->p->size);
    }
}

bool checkHit(position a, position b){
    bool bal  = a.x <= b.x && a.x+1 >= b.x;
    bool jobb = a.x >= b.x && a.x <= b.x+1;
    bool fent = a.y <= b.y && a.y+1 >= b.y;
    bool lent = a.y >= b.y && a.y <= b.y+1;
    return (bal || jobb) && (fent || lent);
}

bool checkDeath(snakeNode* snake1, snakeNode* snake2){
    snakeNode* node = snake2;
    int nodenum = 0;
    while (node != NULL){
        if (node->visible){
            nodenum++;
            if (nodenum>2 && checkHit(node->pos, snake1->pos)){
                return true;
            }
        }
        node = node->next;
    }
    return false;
}

bool checkWallHit(snakeNode* snake, palya p){
    for (int x = 0; x < p.size.x; ++x) {
        for (int y = 0; y < p.size.y; ++y) {
            if (p.palya[x][y] == block && checkHit(snake->pos, (position){x, y}))
                return true;
        }
    }
    return false;
}

position randPos(position max){
    return (position){
            .x = rand() % (int)max.x,
            .y = rand() % (int)max.y
    };
}

void newFood(gameStruct* data){
    data->food = randPos(data->p->size);
    bool badpos = true;
    while (badpos){
        badpos = false;
        for (int x = 0; x < data->p->size.x; ++x) {
            for (int y = 0; y < data->p->size.y; ++y) {
                if (data->p->palya[x][y] == block && checkHit(data->food, (position){x, y})){
                    badpos = true;
                    data->food = randPos(data->p->size);
                }
            }
        }
    }
}

void checkFood(gameStruct* data) {
    for (int i = 0; i < 2 && data->players[i] != NULL ; ++i) { // Minden játékosra
        if (checkHit(data->food, data->players[i]->pos)){ // Felvette-e az ételt
            data->players[i]->score++;
            newFood(data);
        
            snakeNode* node = data->players[i];
            while (node->next != NULL) // utolsó node
                node = node->next;
        
            /*
             * Annyi láthatatlan node-ot hozunk létre, ami az adott mozgással és mintavételezéssel
             * pont egy folytonos kígyót eredményez
             * */
            for (int j = 0; j < (int)((sampletime/speed)); ++j) {
                node->next = malloc(sizeof(snakeNode));
                node->next->prev = node;
                node = node->next;
                node->color = node->prev->color;
                node->pos = node->prev->pos;
                node->direction = node->prev->direction;
                node->visible = false;
            }
            node->next = NULL;
            // Az utolsó látható
            node->visible = true;
        }
    }
    
}

palya* palyaTolt(char* filename){
    FILE* f = fopen(filename, "rt");
    
    palya* palya1 = malloc(sizeof(palya));
    
    if (f == NULL){ // Ha nincs megfelelő file, akkor üres pálya
        perror("File nyitas hiba");
        int size = 40;
        square** p = malloc(size*sizeof(square*));
        for (int x = 0; x < size; ++x) {
            p[x] = malloc(size*sizeof(square));
            for (int y = 0; y < size; ++y) {
                p[x][y]=open;
            }
        }
        *palya1 = (palya){.palya = p, .size = (position){size, size}};
        return palya1;
    }
    
    int maxX = 0;
    int maxY = 0;
    
    int maxScore = 0;
    
    char c;
    int x = 0;
    while (fscanf(f, "%c", &c) != EOF){ // Pálya "megmérése"
        if (c == '\n'){
            maxY++;
            if(maxX < x)
                maxX = x;
            x = 0;
        } else {
            x++;
        }
    }
    maxY++;
    if(maxX < x)
        maxX = x;
    
    // foglalás
    square** p = malloc(maxX* sizeof(square*));
    for (int i = 0; i < maxX; ++i) {
        p[i] = malloc(maxY* sizeof(square));
    }
    
    fseek(f, 0, 0);
    
    x = 0;
    int y = 0;
    while (fscanf(f, "%c", &c) != EOF){ // Pálya tényleges feldolgozása
        if (c == '\n') {
            for (; x < maxX; ++x) {
                p[x][y] = open;
            }
            y++;
            x = 0;
        }else if(isdigit(c)){
            maxScore = maxScore * 10 + (c - '0');
        } else {
            switch (c){
                default:
                    p[x][y] = open;
                    break;
                case 'X':
                    p[x][y] = block;
                    break;
                case 'S':
                    p[x][y] = spawn;
                    break;
            }
            x++;
        }
    }
    fclose(f);
    *palya1 = (palya){.palya = p, .size = (position){.x = maxX, .y = maxY}, .normalModeScore = maxScore};
    return palya1;
}

Uint32 snakeMover(Uint32 interv, void* param){
    /*
     * A mintavételezési sebességgel pakoljuk a kígyó elemeit
     * előre egyesével (láthatatlanokat is beleszámítva)
     * */
    snakeNode* node = (snakeNode*) param;
    while (node->next != NULL)
        node = node->next;
    
    while (node->prev != NULL){
        node->pos = node->prev->pos;
        node = node->prev;
    }
    return interv;
}

double resize(position screen, position size){
    // Megadja, hogy 1 egység (unit) hány px
    double u = screen.y / size.y;
    if (size.x*u > screen.x)
        u = screen.x / size.x;
    return u;
}

void createSnake(int no, gameStruct* p){
    snakeNode* snake = malloc(sizeof(snakeNode));
    snake->color = valid[no];
    snake->direction = (position){0, 0};
    snake->next = NULL;
    snake->prev = NULL;
    snake->visible = true;
    snake->score = 0;
    
    int spawnPoints = 0;
    position start = {
            .x = (double)(rand()%(int)p->p->size.x),
            .y = (double)(rand()%(int)p->p->size.y)
    };
    
    // no-adik valid kezdőpozíció megtalálása, ha nincs, akkor random
    for (int x = 0; x < p->p->size.x && spawnPoints <= no; ++x) {
        for (int y = 0; y < p->p->size.y && spawnPoints <= no; ++y) {
            if (p->p->palya[x][y] == spawn){
                start = (position){x, y};
                spawnPoints++;
            }
        }
    }
    snake->pos=start;
    p->players[no] = snake;
}
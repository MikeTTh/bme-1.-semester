//
// Created by mike on 06/11/2019.
//


#ifndef NHF_LOGIC_H
#define NHF_LOGIC_H

#include <stdbool.h>
#include <SDL2/SDL.h>
#include "snake_common.h"


void moveSnakes(gameStruct* data);

Uint32 snakeMover(Uint32 interval, void* param);

void checkFood(gameStruct* data);

void newFood(gameStruct* data);

palya* palyaTolt(char* filename);

bool checkDeath(snakeNode* snake1, snakeNode* snake2);

double resize(position screen, position size);

void createSnake(int no, gameStruct* data);

bool checkWallHit(snakeNode* snake, palya p);

#endif //NHF_LOGIC_H

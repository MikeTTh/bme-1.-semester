#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "rajzol.h"
#include "snake_common.h"
#include "logic.h"
#include "eventloops.h"
#include "debugmalloc.h"

int main(int argc, char *argv[]);

#ifdef _WIN32
// Enélkül a MinGW nem akarja Windowsra cross-compile-olni nekem a programot
int WinMain(){
    return main(0, NULL);
}
#endif


int main(int argc, char *argv[]) {
    SDL_Renderer* renderer = sdlinit();
    
    TTF_Font* font = TTF_OpenFont("font.ttf", 400);
    if (!font) {
        SDL_Log("Nem sikerult megnyitni a fontot! %s\n", TTF_GetError());
        exit(1);
    }
    
    gameStruct data = initStruct(renderer, font, 0, palyaTolt("palya.txt"));
    data.unit = resize((position){resX, resY}, data.p->size);
    
    while (data.state != done){ // Az event loop és annak azok a részei, amik mindig kellenek
        int start = SDL_GetTicks();
        
        SDL_SetRenderDrawColor(renderer, 0,0,0,255);
        SDL_RenderClear(renderer);
        
        switch (data.state){
            case menu:
                initLoop(&data);
                break;
            case game:
                gameLoop(&data);
                break;
            case win:
                endLoop(&data);
                break;
            case done:
                break;
            default:
                // Ennek nem kéne bekövetkeznie -> valami komoly hiba történt, kilépünk
                data.state = done;
                break;
        }
        
        /* Ha 500+ fps-sel megyünk, akkor valószínűleg nem működik a vsync, ezért,
         * hogy ne együk meg az összes erőforrást,
         * 500 fps-re korlátozom a játékot */
        if (data.lastTick < 2)
            SDL_Delay(2-data.lastTick);
    
        if (data.event.type == SDL_QUIT)
            data.state = done;
    
        data.lastTick = SDL_GetTicks()-start;
        SDL_RenderPresent(data.renderer);
    }
    
    felszabadit(&data);
    
    TTF_CloseFont(data.font);
    SDL_Quit();

    return 0;
}
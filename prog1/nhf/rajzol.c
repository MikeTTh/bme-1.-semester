//
// Created by mike on 04/11/2019.
//

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <time.h>
#include "rajzol.h"
#include "snake_common.h"
#include <stdlib.h>
#include "debugmalloc.h"


bool inRect(SDL_Rect* rect, position mouse){
    return mouse.y > rect->y && mouse.y < rect->y + rect->h && mouse.x > rect->x && mouse.x < rect->x + rect->w;
}

SDL_Renderer* sdlinit(){
    // random inicializálása valamivel jobb értékkel, mint a time(0)
    srand(time(NULL)+clock()/CLOCKS_PER_SEC);
    
    // SDL inicializálása
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        SDL_Log("Nem indithato az SDL: %s", SDL_GetError());
        exit(1);
    }
    
    // Képernyő felbontásának lekérése
    SDL_DisplayMode DM;
    SDL_GetCurrentDisplayMode(0, &DM);
    resX = DM.w;
    resY = DM.h;
    
    // Teljes képernyős ablak létrehozása
    SDL_Window *window = SDL_CreateWindow("Snake", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, resX, resY, SDL_WINDOW_FULLSCREEN_DESKTOP);
    if (window == NULL) {
        SDL_Log("Nem hozhato letre az ablak: %s", SDL_GetError());
        exit(1);
    }
    
    // GPU által gyorsított Vsync-re szabályozott renderer létrehozása
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) {
        SDL_Log("Nem hozhato letre a megjelenito: %s", SDL_GetError());
        exit(1);
    }
    
    TTF_Init();
    
    // Képernyő törlése
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    return renderer;
}

void renderText(SDL_Renderer* renderer, TTF_Font* font, char* string, SDL_Rect* dest, SDL_Color color){
    // Text lerenderelése egy surface-re
    SDL_Surface* surface = TTF_RenderUTF8_Blended(font, string, color);
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
    
    // Megállapjuk,  hogy melyik irányban lógna ki a szöveg a dobozból először
    // és ahhoz megfelelő nagyítási/kicsinyítési szorzót kiszámoljuk
    double scaleX = (double)dest->w/(double)surface->w;
    double scaleY = (double)dest->h/(double)surface->h;
    bool alignToX = scaleX < scaleY;
    double scale = alignToX ? scaleX : scaleY;
    SDL_Rect d = {.x = dest->x, .y = dest->y};
    d.w = (int)(scale * (double)surface->w);
    d.h = (int)(scale * (double)surface->h);
    
    // A doboz közepére igazítjuk a szöveget
    if(alignToX){
        d.y += (int)(((double)dest->h - scale*(double)surface->h)/2.0);
    }else{
        d.x += (int)(((double)dest->w - scale*(double)surface->w)/2.0);
    }
    
    // Elvégezzük a rajzolást
    SDL_RenderCopy(renderer, texture, NULL, &d);
    
    // Felszabadítjuk a már többet nem használt texture-t és surface-t
    SDL_FreeSurface(surface);
    SDL_DestroyTexture(texture);
}

void drawSnakes(gameStruct* data){
    for (int i = 0; i < 2 && data->players[i] != NULL; ++i) { // Minden játékosra
        snakeNode* node = data->players[i];
    
        while (node != NULL){
            if (node->visible){ // Látható elemeket rajzoljuk csak
                SDL_Rect block;
                block.x = (int)(data->unit*node->pos.x);
                block.y = (int)(data->unit*node->pos.y);
                block.h = (int)data->unit;
                block.w = (int)data->unit;
                SDL_SetRenderDrawColor(data->renderer, node->color.r, node->color.g, node->color.b, 255);
                if (node->prev == NULL){ // A kezdő node (fej) telt négyzet, többi nem
                    SDL_RenderFillRect(data->renderer, &block);
                }else{
                    SDL_RenderDrawRect(data->renderer, &block);
                }
            }
            node = node->next;
        }
    }
    
}

void numDraw(SDL_Renderer* render, TTF_Font* font, int score, SDL_Rect* pos, SDL_Color color){
    unsigned long int szjegyek = (score == 0)
                                 ?(2*sizeof(char)) // '0'+'\0'
                                 :(
                                         ((int)log10(score) // 10-esben számjegyek száma - 1
                                          + 2 // + 1 a log felfele kerekítése miatt, + 1 a '\0'
                                         )* sizeof(char)
                                 );
    char* scorestr = malloc(szjegyek);
    
    sprintf(scorestr, "%d", score);
    renderText(render, font, scorestr, pos, color);
    free(scorestr);
}

void drawFood(gameStruct* data){
    // Étel kirajzolása pirosan
    SDL_Rect block;
    block.x = (int)(data->unit*data->food.x);
    block.y = (int)(data->unit*data->food.y);
    block.h = (int)data->unit;
    block.w = (int)data->unit;
    SDL_SetRenderDrawColor(data->renderer, 0xEB, 0x40, 0x34, 255);
    SDL_RenderFillRect(data->renderer, &block);
}

void drawPalya(gameStruct* data){
    // Pálya határa
    SDL_Rect frame;
    frame.x = 0;
    frame.y = 0;
    frame.w = (int)(data->p->size.x*data->unit);
    frame.h = (int)(data->p->size.y*data->unit);
    SDL_SetRenderDrawColor(data->renderer, 255, 255, 255, 255);
    SDL_RenderDrawRect(data->renderer, &frame);
    
    for (int x = 0; x < data->p->size.x; ++x) {
        for (int y = 0; y < data->p->size.y; ++y) {
            // Pályán akadályok kirajzolása
            SDL_Rect rect;
            rect.x = (int)(x * data->unit);
            rect.y = (int)(y * data->unit);
            rect.h = (int)data->unit+1;
            rect.w = (int)data->unit+1;
            switch (data->p->palya[x][y]){
                case block:
                    SDL_SetRenderDrawColor(data->renderer, 255, 255, 255, 255);
                    SDL_RenderFillRect(data->renderer, &rect);
                    break;
                default:
                    break;
            }
            
        }
    }
}

// A gombokat kezelő fv.
void buttonHandler(SDL_Renderer* render, TTF_Font* font, SDL_Rect* place, SDL_Color color, char* text, position mouse, buttonFunc func, void* params){
    SDL_SetRenderDrawColor(render, color.r, color.g, color.b, color.a);
    SDL_RenderDrawRect(render, place);
    renderText(render, font, text, place, (SDL_Color){255, 255, 255, 255});
    if (inRect(place, mouse)){
        func(params);
    }
}

// A gombok kattintására reagáló fv.-ek
void single(void* p){
    gamemode* gm = (gamemode*)p;
    gm->multiplayer = false;
}
void multi(void* p){
    gamemode* gm = (gamemode*)p;
    gm->multiplayer = true;
}
void normal(void* p){
    gamemode* gm = (gamemode*)p;
    gm->death = false;
}
void death(void* p){
    gamemode* gm = (gamemode*)p;
    gm->death = true;
}
void start(void* p){
    bool* done = (bool*) p;
    *done = true;
}

bool rajzKezd(SDL_Renderer* render, gamemode* gm, TTF_Font* lexie, position mouse){
    SDL_SetRenderDrawColor(render, 255, 255, 255, 255);
    int margin = resY / 50;
    int rowH = (resY-margin*3)/4;
    int cellW = (resX-margin*5)/2;
    SDL_Color selected = {0, 255, 0, 255};
    SDL_Color unselected = {255, 255, 255, 255};
    
    // Cím
    SDL_Rect draw = {resX/2-cellW/2, margin, cellW, rowH};
    for (int i = 0; i < 10; ++i) { // Vibráló feliratok
        SDL_Rect rave = draw;
        rave.x += rand()%(resX/50) - resX/100;
        rave.y += rand()%(resY/50) - resY/100;
        renderText(render, lexie, "Sznék", &rave, (SDL_Color){rand()%256, rand()%256, rand()%256, rand()%256});
    }
    // Sima felirat
    renderText(render, lexie, "Sznék", &draw, (SDL_Color){255, 255, 255, 255});
    
    // Gombok
    draw = (SDL_Rect){margin, rowH, cellW, rowH};
    buttonHandler(render, lexie, &draw, (gm->multiplayer)?(unselected):(selected), "Egyjátékos", mouse, single, gm);
    
    draw = (SDL_Rect){cellW+margin*4, rowH, cellW, rowH};
    buttonHandler(render, lexie, &draw, (gm->multiplayer)?(selected):(unselected), "Kétjátékos", mouse, multi, gm);
    
    draw = (SDL_Rect){margin, rowH*2+margin, cellW, rowH};
    buttonHandler(render, lexie, &draw, (gm->death)?(unselected):(selected), "Normál", mouse, normal, gm);
    
    draw = (SDL_Rect){cellW+margin*4, rowH*2+margin, cellW, rowH};
    buttonHandler(render, lexie, &draw, (gm->death)?(selected):(unselected), "Végtelen", mouse, death, gm);
    
    draw = (SDL_Rect){resX/2-cellW/2, rowH*3+margin*2, cellW, rowH};
    bool done = false;
    buttonHandler(render, lexie, &draw, unselected, "Start", mouse, start, &done);
    
    return done;
}

//
// Created by mike on 04/11/2019.
//
#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_ttf.h>
#include "snake_common.h"
#include <stdbool.h>

#ifndef NHF_RAJZOL_H
#define NHF_RAJZOL_H
void numDraw(SDL_Renderer* render, TTF_Font* font, int score, SDL_Rect* pos, SDL_Color color);
SDL_Renderer* sdlinit();
void drawSnakes(gameStruct* data);
bool inRect(SDL_Rect* rect, position mouse);
void drawFood(gameStruct* data);
void drawPalya(gameStruct* data);
void renderText(SDL_Renderer* renderer, TTF_Font* font, char* string, SDL_Rect* dest, SDL_Color color);
bool rajzKezd(SDL_Renderer* render, gamemode* gm, TTF_Font* lexie, position mouse);
typedef void (*buttonFunc)(void*);
void buttonHandler(SDL_Renderer* render, TTF_Font* font, SDL_Rect* place, SDL_Color color, char* text, position mouse, buttonFunc func, void* params);
#endif //NHF_RAJZOL_H

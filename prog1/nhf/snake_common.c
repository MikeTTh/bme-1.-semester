//
// Created by mike on 06/11/2019.
//
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "snake_common.h"
#include "debugmalloc.h"


gameStruct initStruct(SDL_Renderer* renderer, TTF_Font* lexie, double unit, palya* pa){
    // "Alap" játékstruktúra létrehozása
    gameStruct data = {
            .renderer = renderer,
            .font = lexie,
            .event = {.type = SDL_FIRSTEVENT},
            .state = menu,
            .gm = {
                    .death = false,
                    .multiplayer = false
            },
            .lastTick = 0,
            .unit = unit,
            .p = pa,
            .food = {1,1},
            .winner = -1
    };
    data.players[0] = NULL;
    data.players[1] = NULL;
    data.playerTimers[0] = -1;
    data.playerTimers[1] = -1;
    return data;
}

void palyaFelszab(palya* p){
    for (int i = 0; i < p->size.x; ++i) {
        free(p->palya[i]);
    }
    free(p->palya);
    free(p);
}

void snakeFelszab(snakeNode* s){
    snakeNode* n = s->next;
    while (s != NULL){
        free(s);
        s = n;
        if (s != NULL)
            n = s->next;
    }
}

void felszabadit(gameStruct* data){
    for (int i = 0; i<2 && data->players[i] != NULL; ++i) {
        SDL_RemoveTimer(data->playerTimers[i]);
        data->playerTimers[i] = -1;
    }
    palyaFelszab(data->p);
    data->p = NULL;
    for (int i = 0; i < 2 && data->players[i] != NULL; ++i) {
        snakeFelszab(data->players[i]);
        data->players[i] = NULL;
    }
}
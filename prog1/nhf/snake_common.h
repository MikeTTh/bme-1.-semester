//
// Created by mike on 06/11/2019.
//

#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#ifndef NHF_SNAKE_COMMON_H
#define NHF_SNAKE_COMMON_H

int resX;
int resY;
#define speed 15.0
#define sampletime 240.0

typedef enum gameState {
    win,
    game,
    menu,
    done,
} gameState;


typedef struct gamemode {
    bool multiplayer;
    bool death;
} gamemode;

typedef enum {
    open,
    block,
    spawn
} square;

typedef struct {
    double x, y;
} position;

typedef struct snakeNode {
    position direction;
    position pos;
    struct snakeNode* next;
    struct snakeNode* prev;
    SDL_Color color;
    int score;
    bool visible;
} snakeNode;

typedef struct {
    square** palya;
    position size;
    int normalModeScore;
} palya;


typedef struct gameStruct {
    SDL_Renderer* renderer;
    gamemode gm;
    gameState state;
    SDL_Event event;
    TTF_Font* font;
    unsigned int lastTick;
    double unit;
    palya* p;
    snakeNode* players[2];
    SDL_TimerID playerTimers[2];
    position food;
    int winner;
} gameStruct;

gameStruct initStruct(SDL_Renderer* renderer, TTF_Font* lexie, double unit, palya* p);
void palyaFelszab(palya* p);
void snakeFelszab(snakeNode* s);
void felszabadit(gameStruct* data);

#endif //NHF_SNAKE_COMMON_H
